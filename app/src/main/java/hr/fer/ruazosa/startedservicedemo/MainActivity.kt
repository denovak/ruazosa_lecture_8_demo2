package hr.fer.ruazosa.startedservicedemo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    inner class SleepServiceBroadCastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            Toast.makeText(context, "Service wait time passed", Toast.LENGTH_LONG).show()
        }
    }

    val sleepServiceBroadCastReceiver = SleepServiceBroadCastReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val filter = IntentFilter(SleepService.WAIT_TIME_PASSED)
        registerReceiver(sleepServiceBroadCastReceiver, filter)

        startServiceButton.setOnClickListener {
            Intent(this, SleepService::class.java).also { intent ->
                intent.putExtra(SleepService.SECONDS_TO_SLEEP, 5)
                startService(intent)
            }
        }
    }
}
