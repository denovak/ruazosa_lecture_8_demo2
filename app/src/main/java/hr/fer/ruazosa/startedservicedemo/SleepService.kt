package hr.fer.ruazosa.startedservicedemo

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.widget.Toast
import androidx.core.graphics.scaleMatrix

class SleepService : Service() {

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val secondsToSleep = intent?.getIntExtra(SECONDS_TO_SLEEP, 0)
        if (secondsToSleep != null) {
            sendBroadcastAfterWait(secondsToSleep)
        }
        return Service.START_NOT_STICKY
    }

    private fun sendBroadcastAfterWait(secondsToWait: Int) {
        Thread(Runnable {
            Thread.sleep(secondsToWait.toLong()*1000)
            // send broadcast
            Intent().also { intent ->
                intent.setAction(WAIT_TIME_PASSED)
                sendBroadcast(intent)
            }
            stopSelf()
        }).run()
    }

    companion object {
        var SECONDS_TO_SLEEP = "secondsToSleep"
        var WAIT_TIME_PASSED = "hr.fer.ruazosa.startedservicedemo.waittimepassed"
    }

}
